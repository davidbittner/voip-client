use std::net::{UdpSocket, SocketAddr};

lazy_static::lazy_static! {
    pub static ref BUFF_SIZE: usize = 2048;

    pub static ref HOME: UdpSocket = {
        let sock = "127.0.0.1:3332"
            .parse::<SocketAddr>()
            .unwrap();

        UdpSocket::bind(&sock).unwrap()
    };

    pub static ref TO: UdpSocket = {
        let sock = "127.0.0.1:3333"
            .parse::<SocketAddr>()
            .unwrap();

        UdpSocket::bind(&sock).unwrap()
    };
}
