mod conns;

use std::net::{UdpSocket, SocketAddr};
use rodio::Sink;
use voip::packet::Packet;

use std::time::Duration;

use std::thread;
use std::thread::{Thread, JoinHandle};

use std::sync::Mutex;
use std::sync::Arc;

type Arm<T> = Arc<Mutex<T>>;

struct ReceivedPacket(SocketAddr, Packet);

struct ConnState<'a> {
    packets:     Arm<Vec<ReceivedPacket>>,

    buffer:     Arm<Vec<u8>>,
    ref_buffer: Arm<Vec<(usize, SocketAddr)>>,

    term: &'a bool,
}

pub struct Connection<'a> {
    state: ConnState<'a>,

    rec_thr: JoinHandle<()>,
    pro_thr: JoinHandle<()>,
}

impl<'a> Connection<'a> {
    pub fn new() -> Self {
        let rc = ConnState::new();

        let rec_thr = thread::spawn(|| ->() {
        });
        let pro_thr = thread::spawn(|| ->() {
        });

        Self {
            state: rc,

            rec_thr: rec_thr,
            pro_thr: pro_thr,
        }
    }

    pub fn term(mut self) {
        self.state.term = &true;

        self.rec_thr.join()
            .unwrap();

        self.pro_thr.join()
            .unwrap();
    }

    pub fn pop_packet(&mut self) -> Vec<ReceivedPacket> {
        self.state.dump_packets()
    }
}

impl<'a> ConnState<'a> {
    fn new() -> ConnState<'a> {
        ConnState {
            packets:     Arm::default(),
            ref_buffer:  Arm::default(),
            buffer:      Arm::default(),

            term:        &false
        }
    }

    fn rec_loop(mut self) {
        let mut buff = vec![0; *conns::BUFF_SIZE];
        while *self.term {
            let refs = conns::HOME.recv_from(&mut buff)
                .expect("failed to receive packet");

            let mut ref_lock = self.ref_buffer.lock()
                .unwrap();

            let mut buff_lock = self.buffer.lock()
                .unwrap();

            ref_lock.push(refs);
            buff_lock.extend(buff[0..refs.0].iter());
        }
    }

    pub fn dec_loop(mut self) {
        while *self.term {
            let mut swap_buff     = Vec::new();
            let mut swap_ref_buff = Vec::new();

            {
                let mut buff     = self.buffer.lock()
                    .unwrap();
                let mut ref_buff = self.ref_buffer.lock()
                    .unwrap();

                std::mem::swap(&mut swap_buff, &mut buff);
                std::mem::swap(&mut swap_ref_buff, &mut ref_buff);
            }

            for (siz, addr) in swap_ref_buff.into_iter() {
                let packet: Vec<u8> = swap_buff.drain(0..siz)
                    .collect();

                match bincode::deserialize::<Packet>(&packet) {
                    Ok(packet) => {
                        let mut packet_buff = self.packets.lock()
                            .unwrap();

                        packet_buff.push(ReceivedPacket(addr, packet));
                    },
                    Err(err) => eprintln!("failed to deserialize packet: '{}'", err),
                }
            }
        }
    }

    fn dump_packets(&mut self) -> Vec<ReceivedPacket> {
        let mut swap = Vec::new();
        let mut packets = self.packets.lock()
            .unwrap();

        std::mem::swap(&mut swap, &mut packets);
        drop(packets);

        swap
    }
}
