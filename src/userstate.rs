use std::collections::HashMap;
use voip::user::User;

use uuid::Uuid;

use azul::prelude::*;

use crate::listener::Connection;

pub struct UserState<'a> {
    user_map:  HashMap<Uuid, User>,
    voice_map: HashMap<Uuid, rodio::Sink>,

    conn_state: Connection<'a>,
}

impl<'a> UserState<'a> {
    pub fn new() -> Self {
        UserState {
            user_map: HashMap::new(),
            voice_map: HashMap::new(),

            conn_state: Connection::new()
        }
    }
}

impl<'a> Layout for UserState<'a> {
    fn layout(&self, _: LayoutInfo<Self>) -> Dom<Self> {
        Dom::new(NodeType::Div)
    }
}
