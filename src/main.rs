#![feature(duration_float)]
#![forbid(unsafe_code)]

use cpal::EventLoop;
use cpal::StreamData;
use cpal::UnknownTypeInputBuffer;

use std::net::{SocketAddr, UdpSocket};

use bincode::serialize;

use voip::packet::Packet;

use azul::prelude::*;

mod userstate;
mod listener;

pub mod devices {
    use lazy_static::lazy_static;
    lazy_static!{
        pub static ref INPUT_DEV: rodio::Device =
            rodio::default_input_device().unwrap();

        pub static ref OUTPUT_DEV: rodio::Device =
            rodio::default_output_device().unwrap();
    }
}

fn main() -> std::io::Result<()> {
    let app = App::new(userstate::UserState::new(), AppConfig::default());
    let window = Window::new(WindowCreateOptions::default(), css::native()).unwrap();
    app.run(window).unwrap();

    Ok(())
}

/*
let event_loop = EventLoop::new();

let format = devices::INPUT_DEV
    .supported_input_formats()
    .expect("error while querying formats")
    .next()
    .expect("no supported formats")
    .with_max_sample_rate();

let stream = event_loop.build_input_stream(&devices::INPUT_DEV, &format)
    .expect("could not find input device");

event_loop.play_stream(stream);

event_loop.run(move |_id, data| {
    match data {
        StreamData::Input{buffer: UnknownTypeInputBuffer::I16(buffer)} => {
            let mut temp_buffer: Vec<i16> = Vec::new();

            for i in buffer.iter() {
                temp_buffer.push(*i);
            }
        }
       _ => {}
    }
});
*/
